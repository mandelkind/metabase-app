#!/bin/bash
set -eu

# DB ENV
export MB_DB_TYPE="postgres"
export MB_DB_DBNAME=$POSTGRESQL_DATABASE
export MB_DB_USER=$POSTGRESQL_USERNAME
export MB_DB_PASS=$POSTGRESQL_PASSWORD
export MB_DB_HOST=$POSTGRESQL_HOST
export MB_DB_PORT=$POSTGRESQL_PORT

# MAIL ENV
export MB_EMAIL_SMTP_HOST=$MAIL_SMTP_SERVER
export MB_EMAIL_SMTP_PORT=$MAIL_SMTP_PORT
export MB_EMAIL_SMTP_SECURITY=
export MB_EMAIL_SMTP_USERNAME=$MAIL_SMTP_USERNAME
export MB_EMAIL_SMTP_PASSWORD=$MAIL_SMTP_PASSWORD
export MB_EMAIL_FROM_ADDRESS=$MAIL_FROM

# LDAP ENV
export MB_LDAP_ENABLED=true
export MB_LDAP_HOST=$LDAP_SERVER
export MB_LDAP_PORT=$LDAP_PORT
export MB_LDAP_BIND_DN=$LDAP_BIND_DN
export MB_LDAP_PASSWORD=$LDAP_BIND_PASSWORD
export MB_LDAP_USER_BASE=$LDAP_USERS_BASE_DN
export MB_LDAP_USER_FILTER="(|(mail={login})(username={login}))"
export MB_LDAP_ATTRIBUTE_EMAIL="mail"
export MB_LDAP_ATTRIBUTE_FIRSTNAME="givenName"
export MB_LDAP_ATTRIBUTE_LASTNAME=
export MB_LDAP_GROUP_BASE=$LDAP_USERS_BASE_DN

# Setup Java Options
JAVA_OPTS="-XX:+IgnoreUnrecognizedVMOptions"
JAVA_OPTS="${JAVA_OPTS} -Dfile.encoding=UTF-8"
JAVA_OPTS="${JAVA_OPTS} -Dlogfile.path=/app/data/log"
JAVA_OPTS="${JAVA_OPTS} -XX:+CMSClassUnloadingEnabled"              # These two needed for Java 7 to GC dynamically generated classes
JAVA_OPTS="${JAVA_OPTS} -XX:+UseConcMarkSweepGC"
JAVA_OPTS="${JAVA_OPTS} -server"
JAVA_OPTS="${JAVA_OPTS} --add-opens=java.base/java.net=ALL-UNNAMED" # needed for Java 9 to allow dynamic classpath additions -- see https://github.com/tobias/dynapath
JAVA_OPTS="${JAVA_OPTS} --add-modules=java.xml.bind"                # needed for Java 9 (Oracle VM only) because java.xml.bind is no longer on SE classpath by default since it's EE

LIMIT=$(($(cat /sys/fs/cgroup/memory/memory.memsw.limit_in_bytes)/2**20))
JAVA_OPTS="${JAVA_OPTS} -XX:MaxRAM=${LIMIT}M"

# Launch the application
# exec is here twice on purpose to  ensure that metabase runs as PID 1 (the init process)
# and thus receives signals sent to the container. This allows it to shutdown cleanly on exit
exec su www-data -s /bin/sh -c "exec java $JAVA_OPTS -jar /app/code/metabase.jar $@"