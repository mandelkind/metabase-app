FROM cloudron/base:0.12.0
MAINTAINER Marco Betschart <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    	openjdk-8-jdk bash ttf-dejavu fontconfig \
     && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

ENV VERSION 0.27.2
EXPOSE 3000

RUN curl -f -o /tmp/metabase.jar http://downloads.metabase.com/v${VERSION}/metabase.jar \
	&& mv /tmp/metabase.jar /app/code/metabase.jar \
	&& chown www-data:www-data /app/code/metabase.jar

ADD start.sh /app/code/

RUN chown -R www-data:www-data /app/code
RUN chmod +x /app/code/start.sh

CMD [ "/app/code/start.sh" ]