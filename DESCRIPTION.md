This app packages Metabase <upstream>0.27.2</upstream>.

Metabase is the easy, open source way for everyone in your company to ask questions and learn from data.